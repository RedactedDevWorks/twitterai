import markovify
import twitter
from io import StringIO
import json
import re
import time
import sqlite3
import datetime
import time

api = twitter.Api(consumer_key="a1ToF70PvdG0pOEZ808fVI5eO",
                  consumer_secret="7t2kKvLBhLbS0r6ppKj1wfBjOWLKCcmEgVB1IufQQ09alKKGgW",
                  access_token_key="787915148316913669-Bb6DKKz14qzM0zSsFFadvbj2ZnKrmnX",
                  access_token_secret="VAIOuXxXNNjDW7yeP9THUdJdmePL7wObf11CLr3Yk0t2Y")

db = sqlite3.connect(database="store.db")
c = db.cursor()

mentionMatcher = re.compile(r"(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z]+[A-Za-z0-9]+)")

var = 1
while var == 1:

    mentionslimit = api.CheckRateLimit("https://api.twitter.com/1.1/statuses/mentions_timeline.json")
    replylimit = api.CheckRateLimit('https://api.twitter.com/1.1/statuses/update.json')

    c.execute("select tweet_replied from replies order by tweet_replied DESC limit 1")
    replySinceData = c.fetchone()
    reply_details = replySinceData[0] if replySinceData else 0

    print("Rate limits: Mentions: %d, Updates: %d" % (mentionslimit.remaining, replylimit.remaining))

    if replylimit.remaining > 0 and mentionslimit.remaining > 0:
        try:
            statuses = api.GetMentions(since_id=reply_details)
            print("Found %d new replies!" % (len(statuses)))

            for status in statuses:
                c.execute('select tweet_replied from replies WHERE tweet_replied = ?', (status.id,))
                if c.fetchone():
                    continue

                c.execute("select tweets.text from tweets order by random() limit 100")
                replyChoices = c.fetchall()

                text_model = markovify.Text( re.sub(mentionMatcher, '', ' '.join(map(lambda x: x[0], replyChoices))) )
                reply_markov = markovify.Text(status.text)
                model_combo = markovify.combine([reply_markov, text_model], [5, 1])

                # +2 account for @ and space after screen name
                reply = "@%s %s" % (status.user.screen_name,
                                    model_combo.make_short_sentence(100))

                print("Replying to %s with status id %d: %s" % (status.user.screen_name, status.id, reply))

                api.PostUpdate(in_reply_to_status_id=status.id, status=reply)

                # add_to_db(text_to_add=status.text)
                c.execute("insert into replies (tweet_replied, response_text, date_sent) VALUES (?,?,?)",
                          (status.id, reply, int(time.time())))
                db.commit()

                print("Replied, waiting 5 seconds to get to the next mention (to feel more natural)")
                time.sleep(5)

        except twitter.error.TwitterError as err:
            print("Oops, something went.. wrong..: %s" % err.message)
            pass

    else:
        print("Rate Limits Exceeded.. waiting a sec to try again")

    time.sleep(60)


print("Done")
