import twitter
from io import StringIO
import json
import re
import sqlite3

api = twitter.Api(consumer_key="a1ToF70PvdG0pOEZ808fVI5eO",
                  consumer_secret="7t2kKvLBhLbS0r6ppKj1wfBjOWLKCcmEgVB1IufQQ09alKKGgW",
                  access_token_key="787915148316913669-Bb6DKKz14qzM0zSsFFadvbj2ZnKrmnX",
                  access_token_secret="VAIOuXxXNNjDW7yeP9THUdJdmePL7wObf11CLr3Yk0t2Y")

db = sqlite3.connect(database="store.db")
c = db.cursor()

ratelimit = api.CheckRateLimit("https://api.twitter.com/1.1/statuses/user_timeline.json")
print(ratelimit)

buf = StringIO()

# clear out database
c.execute('DELETE FROM tweets')
db.commit()

# Populate database
if ratelimit.remaining > 0:
    if ratelimit.remaining < 5:
        list = range(ratelimit.remaining) # use up the rest
    else:
        list = range(50)

    for i in list:

        c.execute('select tweet_id from tweets order by tweet_id asc limit 1')
        row = c.fetchone()
        max_id = row[0] if row else 0

        statuses = api.GetUserTimeline(screen_name="TheSpoonyOne",
                                       include_rts=False,
                                       exclude_replies=True,
                                       count=50,
                                       max_id=max_id)

        for status in statuses:
            try:
                c.execute('insert into tweets (tweet_id, text, author) VALUES (?,?,?)', (status.id, status.text, status.user.screen_name))
            except sqlite3.IntegrityError as e:
                print("Couldn't persist tweet ID {} for: {}".format(status.id, e.__str__()))

        db.commit()

else:
    print("Rate Limit Exceeded, Aborting")

