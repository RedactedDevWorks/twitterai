import twitter
from twitter import models
import sqlite3
import time
import re
import markovify
from random import randint, choice
import threading
import sys
import traceback


class TwitterActions:

    def __init__(self):
        self.api = twitter.Api(consumer_key="a1ToF70PvdG0pOEZ808fVI5eO",
                               consumer_secret="7t2kKvLBhLbS0r6ppKj1wfBjOWLKCcmEgVB1IufQQ09alKKGgW",
                               access_token_key="787915148316913669-Bb6DKKz14qzM0zSsFFadvbj2ZnKrmnX",
                               access_token_secret="VAIOuXxXNNjDW7yeP9THUdJdmePL7wObf11CLr3Yk0t2Y")

    def rate(self, service):
        """
        Returns the remaining rate limit for a service
        :param str service:
        :return int :
        """
        limit = self.api.CheckRateLimit("https://api.twitter.com/1.1/{}.json".format(service))
        return limit.remaining

    def mentions(self, since_id=None):
        """

        :return list:
        """
        mentions = self.api.GetMentions(since_id=since_id)
        return mentions

    def timeline(self, screen_name, limit, max_id=None):
        """

        :param str screen_name:
        :param int limit:
        :param int max_id:
        :return:
        """
        statuses = self.api.GetUserTimeline(screen_name=screen_name,
                                            include_rts=False,
                                            exclude_replies=True,
                                            count=limit,
                                            max_id=max_id)
        return statuses

    def post(self, text, reply_to=None):
        """

        :param string text:
        :param models.Status reply_to:
        :return:
        """
        return self.api.PostUpdate(in_reply_to_status_id=reply_to, status=text)


    def favorite(self, post):
        """

        :param models.Status post:
        :return:
        """
        pass

    def retweet(self, status):
        """

        :param models.Status status:
        :return:
        """
        pass

    def follow(self, user):
        """

        :param models.User user:
        :return:
        """
        pass

    def follow_random(self):
        """
        Follow a random user going by suggestions
        :return:
        """
        suggestions = self.api.GetUserSuggestionCategories()
        category = choice(suggestions)
        users = self.api.GetUserSuggestion(category=category)
        user = choice(users)
        friendship = self.api.CreateFriendship(user_id=user.id)
        return friendship

    def unfollow(self, user):
        """

        :param models.User user:
        :return:
        """
        pass

    def unfollow_random(self):
        """
        Choose random person from follow list to unfollow.
        Gives the impression the friends list is dynamic
        :return:
        """
        pass


class Query:
    def __init__(self, store="store.db"):
        self.connection = sqlite3.connect(database=store)
        self.cursor = self.connection.cursor()

    def truncate(self, table):
        """

        :param str table:
        :return:
        """
        self.cursor.execute('DELETE FROM {}'.format(table))
        return self.connection.commit()

    def insert_reply(self, tweet_replied, response_text):
        """

        :param int tweet_replied:
        :param str response_text:
        :return:
        """
        self.cursor.execute("INSERT INTO replies (tweet_replied, response_text, date_sent) VALUES (?,?,?)",
                            (tweet_replied, response_text, int(time.time())))
        self.connection.commit()
        return

    def insert_tweet(self, tweet_id, text, author):
        """

        :param int tweet_id:
        :param str text:
        :param str author:
        :return:
        """
        try:
            self.cursor.execute('insert into tweets (tweet_id, text, author) VALUES (?,?,?)',
                                (tweet_id, text, author))
            self.connection.commit()
        except sqlite3.IntegrityError as e:
            print("Couldn't persist tweet ID {} for: {}".format(tweet_id, e.__str__()))

        pass

    def fetch_latest_reply_id(self):
        """

        :return int:
        """
        self.cursor.execute("select tweet_replied from replies order by tweet_replied DESC limit 1")
        row = self.cursor.fetchone()
        return row[0] if row else 0

    def fetch_latest_banked_status_id(self):
        self.cursor.execute('select tweet_id from tweets order by tweet_id asc limit 1')
        row = self.cursor.fetchone()
        max_id = row[0] if row else 0
        return max_id

    def fetch_random_tweets(self, limit=100):
        self.cursor.execute("select tweets.text from tweets order by random() limit {}".format(limit))
        replyChoices = self.cursor.fetchall()
        return replyChoices

    def has_replied(self, id):
        """
        :param int id:
        :return bool:
        """
        self.cursor.execute('select tweet_replied from replies WHERE tweet_replied = ?', (id,))
        return True if self.cursor.fetchone() else False


class MarkovTools:
    def __init__(self, query):
        """
        :param Query query:
        """
        self.query = query
        self.mentionMatcher = re.compile(r"(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z]+[A-Za-z0-9]+)")

    def random_blob(self, limit=100):
        """

        :param limit:
        :return:
        """
        data = self.query.fetch_random_tweets(limit=limit)
        blob = re.sub(self.mentionMatcher, '', ' '.join(map(lambda x: x[0], data)))
        mark_text = markovify.Text(blob)
        return mark_text

    def create_response(self, text_sent, weight=5):
        blob = self.random_blob()
        reply_text = markovify.Text(text_sent)
        mark_model = markovify.combine([blob, reply_text], [1, 5])
        return mark_model


def db():
    """
    Refreshes the Database
    :return:
    """

    query = Query()
    actions = TwitterActions()

    # screen_names = ['christinefriar']
    screen_names = ['TheSpoonyOne']

    var = 1
    while var == 1:
        try :
            print("[DB] Starting routine")

            rate_limit = actions.rate('statuses/user_timeline')
            if rate_limit > 0:
                query.truncate('tweets')
                if rate_limit < 5:
                    list = range(rate_limit)  # use up the rest
                else:
                    list = range(50)

                for i in list:
                    max_id = query.fetch_latest_banked_status_id()
                    statuses = actions.timeline(screen_name=choice(screen_names), limit=50, max_id=max_id)
                    for status in statuses:
                        query.insert_tweet(status.id, status.text, status.user.screen_name)

        except BaseException as error:
            print("[DB] Something dastardly happened, {}".format(error))
            print(traceback.format_exc())

        sleepfor = randint(1800, 4000)
        print("[DB] Resting for: {} seconds".format(sleepfor))
        time.sleep(sleepfor)


def post():
    query = Query()
    actions = TwitterActions()
    markov = MarkovTools(query=query)

    # starting sleep
    sleepfor = randint(30, 90)
    print("[Post] Resting for: {} seconds".format(sleepfor))
    time.sleep(sleepfor)

    var = 1
    while var == 1:
        try:
            print("[Post] Starting routine")

            if actions.rate('statuses/update'):
                text = markov.random_blob(100).make_short_sentence(char_limit=100)
                # Only post something if we were able to craft a status update
                if text:
                    actions.post(text)

        except BaseException as error:
            print("[Post] Something dastardly happened, {}".format(error))
            print(traceback.format_exc())

        sleepfor = randint(30, 1800)
        print("[Post] Resting for: {} seconds".format(sleepfor))
        time.sleep(sleepfor)


def reply():

    action = TwitterActions()
    query = Query()
    markov = MarkovTools(query=query)

    var = 1
    while var == 1:
        try:
            print("[Reply] Starting routine")
            # can we even poll for mentions right now?
            mentions_rate = action.rate('statuses/mentions_timeline')
            if mentions_rate > 0:
                # get our latest reply id
                since_id = query.fetch_latest_reply_id()
                # go through each reply (if there are any)
                mentions = action.mentions(since_id=since_id)
                print("[Reply] Found {} replies".format(len(mentions)))
                index = -1
                for status in mentions:
                    index += 1
                    # check to meke sure we havent already replied to this one, and we can even reply in the first place
                    if not query.has_replied(id=status.id) and action.rate('statuses/update'):
                        reply_text = markov.create_response(text_sent=status.text)
                        # only if we could craft a status update, do we do something, otherwise well just try again
                        if reply_text:
                            reply_text = "@{} {}".format(status.user.screen_name, reply_text.make_short_sentence(100))
                            action.post(text=reply_text, reply_to=status.id)
                            query.insert_reply(tweet_replied=status.id, response_text=reply_text)

                            print("[Reply] Replied to Status ID {} with '{}'".format(status.id, reply_text))

                            # If we have any more mentions to go through,
                            # let's delay to make it look like we're writing a disappointing zinger
                            if index + 1 < len(mentions):
                                time_to_next_response = randint(5, 20)
                                print("[Reply] Crafting the next response in {} seconds".format(time_to_next_response))
                                time.sleep(time_to_next_response)
        except BaseException as error:
            print("[Reply] Something dastardly happened, {}".format(error))
            print(traceback.format_exc())

        sleepfor = randint(15, 60)
        print("[Reply] Resting for: {} seconds".format(sleepfor))
        time.sleep(sleepfor)


def follow():

    actions = TwitterActions()

    var = 1
    while var == 1:
        try:
            print("[Follow] Starting Routine")
            user = actions.follow_random()
            print("[Follow] Just Followed user {} ({}) :: {}".format(user.name, user.screen_name, user.id))

        except BaseException as error:
            print("[Follow] Something dastardly happened, {}".format(error))
            print(traceback.format_exc())

        sleepfor = randint(30, 1800)
        print("[Follow] Resting for: {} seconds".format(sleepfor))
        time.sleep(sleepfor)


t_db = threading.Thread(target=db)
t_post = threading.Thread(target=post)
t_reply = threading.Thread(target=reply)
t_follow = threading.Thread(target=follow)

t_db.start()
t_post.start()
t_reply.start()
t_follow.start()

t_db.join()
t_post.join()
t_reply.join()
t_follow.join()
